module.exports.main = function (msg) {
    const fs = require(`fs`)
    const p = require(`../../config.json`).Prefix

    if (msg.content.startsWith(p)) {
        let Commands = fs.readdirSync(`./Events/message/Commands`)
        let string = msg.content.split(p)[1].split(` `)[0].toLocaleLowerCase() + '.js'

        if (Commands.includes(string)) {
            require(`./Commands/${string}`).main(msg, p)
        }
    }
}