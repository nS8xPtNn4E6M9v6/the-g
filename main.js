const Discord = require('discord.js')
const Client = new Discord.Client()

const fs = require('fs')
const config = require('./config.json')

Events = fs.readdirSync(`./Events`)

Events.forEach(i => {
    Client.on(i, (x, y, z) => {
        require(`./Events/${i}/main.js`).main(x, y, z)
    })
});

Client.on('message', msg => {
    console.log(msg.guild.roles.map(r => `${r.name} - ${r.id}\n`))
})

Client.login(config.Token)